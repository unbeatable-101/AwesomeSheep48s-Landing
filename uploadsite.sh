#/bin/sh

bundle exec jekyll b
cp -R _site /usr/local/var/www/
cp -R /usr/local/var/www/AwesomeSheep48s-Landing /usr/local/var/www/AwesomeSheep48s-Landing.backup
rm -rf /usr/local/var/www/AwesomeSheep48s-Landing/*
mv /usr/local/var/www/_site/* /usr/local/var/www/AwesomeSheep48s-Landing
rm -r /usr/local/var/www/_site/
