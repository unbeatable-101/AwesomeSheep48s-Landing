---
layout: post
title: Setting Up My Blog
---

## Hello World!

I've had a website since september, but never had more than a few links on the front page. Yesterday I decided that I should do more with my site and thought to start a blog.

I've been reading [Kev Quirk's blog](https://kevq.uk/), and knew that he uses Jekyll, so I thought that would be a good starting point and got to work looking for a theme; what I wanted was something nice and simple, so when I saw [no style, please!](https://github.com/riggraz/no-style-please) on one of the theme browsers, it jumped out to me, especially since it supports darkmode without me needing to change anything.

------

And yeah. that's about it. I'll probably be writting more on my blog, so if you want to follow it, here is the [Atom feed](../feed.xml). Bye for now!
