---
layout: post
title: Snow, Lots and Lots of Snow
---

## Everyone loves a little bit of snow, eh?

Emphasis on *little*, you probably would start to rethink your love of snow if like me and the rest of Southern Ontario, you awoke yesterday to find 30-50 cm on the ground (I still love snow though). For myself however, the snow didn't really affect me, other than the 3 hours of shovelling that it took to clear our driveway plus a few of our neighbours and a small stretch of sidewalk. Schools were closed due to almost nobody being able to get there, which was good for me as I had chosen that day to sleep past my alarm.

If you want some idea of what the storm looked like, here is a [news article](https://www.cbc.ca/news/canada/winter-weather-ontario-snow-jan17-2022-1.6317370) along with some pictures.

## The shovelling begins

Me and my dad started shovelling at ~09:30 and continued until ~12:30, most of our neighbourhood was out too, helping eachother shovel their driveways (not that it did them much good, as the snow plough didn't come until almost dusk) and the sidewalk, there were even a few people riding snowmobiles on the road.

<figure>
<img src="/assets/lots-of-snow.webp" alt="A road covered in snow, with snowmobile tracks on top. You can also see a shovelled driveway with mounds of snow beside it" />
<figcaption> View from my top floor after shovelling the driveway, you can see snowmobile tracks on the road</figcaption>
</figure>

My two dogs, Leia and Penny, also were enjoying themselves in the snow with a few of the other dogs, jumping and ploughing through the snow on the road, though Leia wasn't happy that there was no grass to pee on.

After the shovelling was done, I basically came inside and collapsed with a book and played Flight Sim 2020 for a few hours, not working on any of my school
projects that I had. Eventually the plough came by, which meant we had to go out and shovel a path to the road, but it wasn't too much work.

## Today

Not much has happened today, except for school being cancelled again because the sidewalk ploughs still haven't made it out to clear the snow and the highways still being closed in the mornings. I did manage to work on some of my projects and start painting my room though.

---

This is day 3 of [#100DaysToOffload](https://100daystooffload.com/)

