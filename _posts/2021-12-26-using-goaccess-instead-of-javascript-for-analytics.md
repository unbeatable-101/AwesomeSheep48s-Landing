---
layout: post
title: Using GoAccess Instead of JavaScript Based Analytics
---

As with [my first post](../setting-up-my-blog), I was reading [Kev Quirk's blog](https://kevq.uk/) – specifically the post about [Netlify Analytics](https://kevq.uk/testing-netlify-analytics) – and it got me thinking; surely there must be a way to set something similar to this up on my server instead of using goatcounter, which I had just set up on December 23rd to give me some kind of insight into how many people were reading my posts[^1].

What I tried first was a simple bash alias: `alias readlog="tail -fn15 /var/log/caddy/main.log |jq 'del(.resp_headers)'"`, which was fine for a cursitory glance, but not really what I was looking for as it meant reading lines of JSON.

Then yesterday, when I was checking [Fosstodon](https://fosstodon.org), I saw that [https://jeffhuang.com/designed_to_last/](https://jeffhuang.com/designed_to_last/) had come up in the [Hacker News](https://news.ycombinator.com/) top 100 again[^2], and since I had just started a blog, I decided to give it another read.

I'm glad that I did, section 4 gave me exactly what I was looking for
<blockquote>End all forms of hotlinking […] Google Analytics is unnecessary; store your own server logs and set up GoAccess or cut them up however you like, giving you more detailed statistics.</blockquote>
I went to the website for [GoAccess](https://goaccess.io/), read about it a bit, and decided to install it without delay[^3]: I could run it on my server and get a nice dashboard, all without any JS on my site, or third-party getting access to my logs.

After installation, I changed my bash alias to `alias readlog="goaccess /var/log/caddy/main.log"`, as well as edited `/etc/goaccess/goaccess.conf` to uncomment `log-format CADDY`. Here is what it looks like when I run it:
![Picture of GoAccess showing analytics of my server](/assets/GoAccess.webp)

## Conclusion

Whilst something like Google analytics might be able to offer you more data or tie it together with your advertisements, none of that applies to me, either pragmatically or philosophically. If you already have the logs on your machine, why not use them as your analytics?

---

This is day 2 of [#100DaysToOffload](https://100daystooffload.com/)

---

[^1]: Not that I need to know or am going to blog based on it, I was just curious.

[^2]: I follow @hn100@botsin.space on Fosstodon

[^3]: If you are using caddy on Ubuntu or Debian, I recommend using [GoAccess' repo](https://goaccess.io/download#official-repo), as it is more up-to-date with support for the caddy log format
