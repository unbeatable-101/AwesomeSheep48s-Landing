---
layout: post
title: "Going Back to School"
---
Well, today was the first day of school in Ontario, and like usual it was pretty boring, just all my teachers introducing themselves and making you fill out those student surveys. That's not to say the boringness will last long though, since I have a pretty busy schedule this semester of French, Physics, Functions, and Chemistry.

Anyway I don't have much more to say, other than that I will be trying to write more on my blog to beat #100DaysToOffload, au revoir!

----

This is day 1 of [#100DaysToOffload](https://100daystooffload.com/)
(I restarted since it has been too long in my opinion)

---
