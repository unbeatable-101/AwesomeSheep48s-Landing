---
layout: post
title: "Setting Up and Running My Own Matrix Server With Conduit"
---

I personally believe that the web should be more decentralized, and use [matrix](https://matrix.org/)[^1] for most of my online communication, as such, it seemed only natural that I should run my own server. The only question: Which one? Synapse was out of the question as it was too much of a resource hog for my poor free Microsoft Azure VM that I got with the GitHub student pack, and I had tried to use dendrite before, but found it confusing to set up, so I was already predisposed against it.

## Enter Conduit
[Conduit](https://conduit.rs) is a pretty performant and low footprint server, aimed at low user-count instances, which seemed perfect for my single user + possible future bridges plan of what I'd do with my server. It also helped that I was (and still am) trying to learn rust at the time, so it was nice to think that I might actually be able to help if something went wrong (it *is* still in beta after all).

### My first attempt
[Conduit's written documentation](https://gitlab.com/famedly/conduit/-/blob/next/DEPLOY.md) is fairly good if you just want to install the master (official release) or next (main development) branches. Since I was doing this in early September 2021, when v0.2.0 had just come out, I went with the master branch and followed the instructions.

It was all going well until I tried setting up https on [nginx](https://nginx.org/en/)[^2], and oops, I got rate limited by Let's Encrypt for too many failed verifications in an hour (I completely blame myself for this). There was something wrong with how I had set up the ssl settings in the config file, and since I honestly barely knew what I was doing, I just kept trying again and again. Thankfully the people in [#conduit:fachschaften.org](https://matrix.to/#/#conduit:fachschaften.org) were kind enough to point me to [Caddy](https://caddyserver.com/), which offers automatic https encryption setup, and has a very simple reverse-proxy syntax (all you really need is three lines of a formatted config file).

### My second attempt with Caddy
Setting up Caddy with conduit was *very* easy, at first all I had were these three lines
```
matrix.awesomesheep48.me, matrix.awesomesheep48.me:8448 {
	reverse_proxy http://127.0.0.1:6167
}
```
Which now looks like this
```
matrix.awesomesheep48.me, matrix.awesomesheep48.me:8448 {
	header "/.well-known/matrix/*" Content-Type application/json #to fix a thunderbird bug where this is needed
	header "/.well-known/matrix/*" Access-Control-Allow-Origin "*" #to fix a thunderbird bug where this is needed
	respond /.well-known/matrix/client {"m.homeserver":{"base_url":"https://matrix.awesomesheep48.me"}} #to fix a thunderbird bug where this is needed
	import snippets/global
	reverse_proxy http://127.0.0.1:6167 {
		header_down -x-frame-options #contained in snippets/global
		header_down -server #So I don't have server: rocket and server: caddy
	}
	respond / "It Works!"
}
```
And here is `snippets/global` for anyone curious
```
templates #for the 'handle errors' section below
uri strip_suffix .html
try_files {path}.html {path}
header {
Strict-Transport-Security "max-age=31536000; includeSubdomains; preload"
X-Frame-Options DENY
X-XSS-Protection "1; mode=block"
Referrer-Policy no-referrer
Permissions-Policy interest-cohort=()
X-Content-Type-Options nosniff
}
handle_errors {
	respond "{http.error.status_code} {http.error.status_text}"
}
encode zstd gzip #zstd is better, but not every browser supports it
```
Ever since setting up caddy, I haven't had any problems with it, and conduit has also been very good – at least until I decided to start being more adventurous with installing the testing branches.

## The Community
The main developer of Conduit, [Timo Kösters](https://gitlab.com/timokoesters) is very accepting of patches and an active maintainer.

When there is a new feature being tested (such as using rocksdb instead of sqlite for better performance), it is announced in [#conduit:fachschaften.org](https://matrix.to/#/#conduit:fachschaften.org), and Timo works to help debug it.

## Shortfalls and Next Steps
Of course it isn't all sunshine and rainbows running conduit: When you set up you server, [DEPLOY.md](https://gitlab.com/famedly/conduit/-/blob/next/DEPLOY.md) doesn't mention how to use a [`.well-known`](https://spec.matrix.org/unstable/client-server-api/#well-known-uri)[^3] file to have your root domain appear as the server in matrix, and since conduit doesn't yet have backfill support (loading messages from before the server joined a room), if you don't do it at the start or soon after, you are stuck if you want to keep your messages (a sort of sunk cost fallacy).

If you want to set it up on your own conduit server, set `server_name` in your `conduit.toml` file to your root domain, and here is an exmaple caddy config file for it

```
example.com {
        header "/.well-known/matrix/*" Content-Type application/json
        header "/.well-known/matrix/*" Access-Control-Allow-Origin "*"
        respond /.well-known/matrix/server {"m.server":"matrix.example.com:8448"}
        respond /.well-known/matrix/client {"m.homeserver":{"base_url":"https://matrix.example.com"}}
}

matrix.example.com matrix.example.com:8448 {
        reverse_proxy http://127.0.0.1:6167
}
```

Conduit is also missing some features, such as backfill (as I mentioned before), verification of accounts over federation, link embeds, and mainline room v1,2,3,4,7,8, and 9 support (versions 2,3,4,7,8, and 9 currently have working, if a little broken, testing branches).

----

This is day 1 of [#100DaysToOffload](https://100daystooffload.com/)

---
[^1]: If you have never used matrix before, think of it like a federated discord, anyone can run their own server, which can commuicate with other servers.
[^2]: Nginx is a pretty standard [reverse proxy](https://en.wikipedia.org/wiki/Reverse_proxy), and has an extensive configuation, but along with that it can be hard to set up if you haven't done that before.
[^3]: This is used to redirect matrix clients and servers from your main domain.tld to your matrix.domain.tld, so that in matrix, your username will appear as `@username:example.com` instead of `@username:matrix.example.com`
