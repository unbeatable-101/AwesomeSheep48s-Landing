---
layout: post
title: Small Mistakes
---
About two months ago (from Dec 19, 2021, 21:01 – 21:06 to be exact), I accidentally changed the permissions of every file that didn't start with `.` in my home directory to read only, I honestly don't remember how I did it, just that I quickly fixed it for all visible folders.

Now something you might not now about me is that I run three bots on [https://newsbots.eu](https://newsbots.eu): [@CBCTopStories@newsbots.eu](https://newsbots.eu/@CBCTopStories), [@CBCScienceEnv@newsbots.eu](https://newsbots.eu/@CBCScienceEnv), and [@CBCCanada@newsbots.eu](https://newsbots.eu/@CBCCanada), all relaying said RSS feeds to Mastodon. I also keep the binary[^1] for the bots stored in a git repo in my home directory, though hidden from view in the GUI since I was too lazy to rewrite the launchd plists and move the binary to a proper location. This is important since when I was fixing the permissions change that I accidentally did, I did it using the GUI and consequently forgot to do it for the hidden git repo. It was also around this time when I started reading more in my RSS reader and so I didn't really have much use for the bot apart from being able to retoot news articles. 

All these factors put together meant that from the night that I messed up my permissions until tonight, the bots had been offline and I hadn't even noticed.

---

This is day 4 of [#100DaysToOffload](https://100daystooffload.com/)

---

[^1]: [feed2toot](https://gitlab.com/chaica/feed2toot/) is the program that I use
